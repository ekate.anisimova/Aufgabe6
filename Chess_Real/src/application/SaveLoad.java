package application;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class SaveLoad {

	public static void write(ObservableList<Eröffnung> eröffnungListe) {
		try {
			FileOutputStream fos = new FileOutputStream("EröffnungObjekt.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(new ArrayList<Eröffnung>(eröffnungListe));
			oos.close();
			System.out.println("Datei erfolgreich gespeichert.");
		} catch (Exception e) {
			System.out.println("Datei speichern fehlgeschlagen: " + e.getMessage());
		}
	}

	public static void read() {
		try {
			Path path = Paths.get("EröffnungObjekt.ser");
			InputStream in = Files.newInputStream(path);
			ObjectInputStream ois = new ObjectInputStream(in);

			List<Eröffnung> list = (List<Eröffnung>) ois.readObject();
			Eröffnungstabelle.eröffnungListe = FXCollections.observableList(list);
			Eröffnungstabelle.tabelle.setItems(Eröffnungstabelle.eröffnungListe);

			System.out.println("Eröffnungsliste erfolgreich geladen.");
		} catch (Exception e) {
			System.out.println("Laden der Eröffnungsliste fehlgeschlagen: " + e.getMessage());
		}
	}
}
