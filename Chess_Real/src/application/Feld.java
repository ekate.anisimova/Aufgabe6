package application;

import static application.Schachbrett.FELDGR��E;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Feld extends Rectangle {
	private Figur figur;

	// Konstruktor
	public Feld(boolean hell, int x, int y) {
		setWidth(FELDGR��E);
		setHeight(FELDGR��E);

		relocate(x * FELDGR��E, y * FELDGR��E);
		setFill(hell ? Color.valueOf("#FFD6AB") : Color.valueOf("#A8713D"));
	}

	// Getter & Setter
	public Figur getFigur() {
		return figur;
	}

	public void setFigur(Figur figur) {
		this.figur = figur;
	}

	public boolean hasFigur() {
		return figur != null;
	}
}
