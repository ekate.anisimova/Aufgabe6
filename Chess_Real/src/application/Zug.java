package application;

import java.io.Serializable;

public class Zug implements Serializable {
	int startX;
	int startY;
	int zielX;
	int zielY;

	private static final long serialVersionUID = 4646991253029518474L;

	// Konstruktor
	public Zug(int startX, int startY, int zielX, int zielY) {
		super();
		this.startX = startX;
		this.startY = startY;
		this.zielX = zielX;
		this.zielY = zielY;
	}

	// Getter
	public int getStartX() {
		return startX;
	}

	public int getStartY() {
		return startY;
	}

	public int getZielX() {
		return zielX;
	}

	public int getZielY() {
		return zielY;
	}
}
