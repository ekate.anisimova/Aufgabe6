package application;

import static application.Schachbrett.FELDGR��E;

import java.io.Serializable;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class Figur extends Pane implements Serializable {
	private FigurTyp figurtyp;
	boolean figurWeiss = true;
	private double mouseX, mouseY;
	private double oldX, oldY;

	private static final long serialVersionUID = -171593151913983770L;

	// Konstruktor
	public Figur(FigurTyp figurtyp, boolean figurWeiss, int x, int y, boolean editor) {
		this.figurtyp = figurtyp;
		this.figurWeiss = figurWeiss;
		move(x, y);

		Image imgFigur;
		if (figurWeiss) {
			imgFigur = new Image("/res/w_" + figurtyp + ".png", true);
		} else {
			imgFigur = new Image("/res/b_" + figurtyp + ".png", true);
		}

		ImageView imgViewFigur = new ImageView();
		imgViewFigur.setImage(imgFigur);

		getChildren().addAll(imgViewFigur);

		if (editor) {
			setOnMousePressed(e -> {
				mouseX = e.getSceneX();
				mouseY = e.getSceneY();
			});

			setOnMouseDragged(e -> {
				relocate(e.getSceneX() - mouseX + oldX, e.getSceneY() - mouseY + oldY);
			});
		}
	}

	// Getter & Setter
	public FigurTyp getFigurTyp() {
		return figurtyp;
	}

	public boolean isFigurWeiss() {
		return figurWeiss;
	}

	public double getOldX() {
		return oldX;
	}

	public double getOldY() {
		return oldY;
	}

	public void move(int x, int y) {
		oldX = x * FELDGR��E;
		oldY = y * FELDGR��E;
		relocate(oldX, oldY);
	}

	public void moveAbbrechen() {
		relocate(oldX, oldY);
	}
}
