package application;

public class MoveErgebnis {
	private MoveTyp moveTyp;
	private Figur figur;

	// Konstruktor
	public MoveErgebnis(MoveTyp moveTyp, Figur figur) {
		this.moveTyp = moveTyp;
		this.figur = figur;
	}

	public MoveErgebnis(MoveTyp moveTyp) {
		this(moveTyp, null);
	}

	// Getter
	public MoveTyp getMoveTyp() {
		return moveTyp;
	}

	public Figur getFigur() {
		return figur;
	}
}
