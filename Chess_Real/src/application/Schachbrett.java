package application;

import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.layout.Pane;

public class Schachbrett {
	public static final int FELDGR��E = 65;
	public static final int BREITE = 8;
	public static final int H�HE = 8;

	public static Parent brettErstellen(Feld[][] brett, Group feldGroup, Group figurGroup, boolean editor) {
		Pane root = new Pane();
		root.setPrefSize(BREITE * FELDGR��E, H�HE * FELDGR��E);
		root.getChildren().addAll(feldGroup, figurGroup);

		// Brett und Figuren
		for (int x = 0; x < BREITE; x++) {
			for (int y = 0; y < H�HE; y++) {
				// Feld
				Feld feld = new Feld((x + y) % 2 == 0, x, y);
				brett[x][y] = feld;
				feldGroup.getChildren().add(feld);

				// Figuren
				Figur figur = null;
				// Schwarz
				if ((x == 0 || x == 7) && y == 0) {
					figur = figurErstellen(FigurTyp.turm, false, x, y, figurGroup, brett, editor);
				} else if ((x == 1 || x == 6) && y == 0) {
					figur = figurErstellen(FigurTyp.springer, false, x, y, figurGroup, brett, editor);
				} else if ((x == 2 || x == 5) && y == 0) {
					figur = figurErstellen(FigurTyp.l�ufer, false, x, y, figurGroup, brett, editor);
				} else if (x == 3 && y == 0) {
					figur = figurErstellen(FigurTyp.dame, false, x, y, figurGroup, brett, editor);
				} else if (x == 4 && y == 0) {
					figur = figurErstellen(FigurTyp.k�nig, false, x, y, figurGroup, brett, editor);
				} else if (y == 1) {
					figur = figurErstellen(FigurTyp.bauer, false, x, y, figurGroup, brett, editor);
				}
				// Wei�
				if ((x == 0 || x == 7) && y == 7) {
					figur = figurErstellen(FigurTyp.turm, true, x, y, figurGroup, brett, editor);
				} else if ((x == 1 || x == 6) && y == 7) {
					figur = figurErstellen(FigurTyp.springer, true, x, y, figurGroup, brett, editor);
				} else if ((x == 2 || x == 5) && y == 7) {
					figur = figurErstellen(FigurTyp.l�ufer, true, x, y, figurGroup, brett, editor);
				} else if (x == 3 && y == 7) {
					figur = figurErstellen(FigurTyp.dame, true, x, y, figurGroup, brett, editor);
				} else if (x == 4 && y == 7) {
					figur = figurErstellen(FigurTyp.k�nig, true, x, y, figurGroup, brett, editor);
				} else if (y == 6) {
					figur = figurErstellen(FigurTyp.bauer, true, x, y, figurGroup, brett, editor);
				}

				// Figur auf das Feld setzen und in figurGroup hinzuf�gen
				if (figur != null) {
					feld.setFigur(figur);
					figurGroup.getChildren().add(figur);
				}
			}
		}

		return root;
	}

	public static int zumBrett(double pixel) {
		return (int) (pixel + FELDGR��E / 2) / FELDGR��E;
	}

	public static MoveErgebnis tryMove(Figur figur, int newX, int newY, Feld[][] brett) {
		if (brett[newX][newY].hasFigur() && brett[newX][newY].getFigur().isFigurWeiss() == figur.isFigurWeiss()) {
			return new MoveErgebnis(MoveTyp.NICHTS);
		}

		if (brett[newX][newY].hasFigur() && brett[newX][newY].getFigur().isFigurWeiss() != figur.isFigurWeiss()) {
			return new MoveErgebnis(MoveTyp.SCHLAGEN, brett[newX][newY].getFigur());
		}

		return new MoveErgebnis(MoveTyp.NORMAL);
	}

	public static Figur figurErstellen(FigurTyp figurtyp, boolean figurWeiss, int x, int y, Group figurGroup,
			Feld[][] brett, boolean editor) {
		Figur figur = new Figur(figurtyp, figurWeiss, x, y, editor);
		brett[x][y].setFigur(figur);

		if (editor) {
			figur.setOnMouseReleased(e -> {
				int newX = zumBrett(figur.getLayoutX());
				int newY = zumBrett(figur.getLayoutY());

				MoveErgebnis ergebnis = tryMove(figur, newX, newY, brett);

				int x0 = zumBrett(figur.getOldX());
				int y0 = zumBrett(figur.getOldY());

				switch (ergebnis.getMoveTyp()) {
				case NICHTS:
					figur.moveAbbrechen();
					break;
				case NORMAL:
					figur.move(newX, newY);
					brett[x0][y0].setFigur(null);
					brett[newX][newY].setFigur(figur);

					// Zug in die Er�ffnung hinzuf�gen
					Main.schachEditor.addZ�geDerEr�ffnung(x0, y0, newX, newY);

					break;
				case SCHLAGEN:
					figur.move(newX, newY);
					brett[x0][y0].setFigur(null);

					// Figur schlagen und ersetzen
					Figur andereFigur = ergebnis.getFigur();
					brett[zumBrett(andereFigur.getOldX())][zumBrett(andereFigur.getOldY())].setFigur(null);
					figurGroup.getChildren().remove(andereFigur);
					brett[newX][newY].setFigur(figur);

					// Zug Speichern
					Main.schachEditor.addZ�geDerEr�ffnung(x0, y0, newX, newY);
					Er�ffnung.figurFriedhof.put(Main.schachEditor.z�geDerEr�ffnung.size() - 1, andereFigur);

					break;
				}
			});
		}
		return figur;
	}
}
