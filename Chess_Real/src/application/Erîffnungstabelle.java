package application;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class Eröffnungstabelle {
	public static TableView<Eröffnung> tabelle;
	public static ObservableList<Eröffnung> eröffnungListe;
	public static Eröffnung wahlEröffnung;
	private Eröffnung alteChooseEröffnung;

	public TableView<Eröffnung> erstelleTabelle() {
		tabelle = new TableView<Eröffnung>();
		tabelle.setPrefSize(270, 520);
		tabelle.relocate(30, 50);

		TableColumn<Eröffnung, String> nameColumn = new TableColumn<>("Eröffnung"); // Header
		nameColumn.setMinWidth(268);
		nameColumn.setResizable(false);
		nameColumn.setCellValueFactory(new PropertyValueFactory<>("name")); // variable
		tabelle.getColumns().add(nameColumn);

		// Eventhandler
		tabelle.setRowFactory(tv -> {
			// Eventhandler
			TableRow<Eröffnung> row = new TableRow<>();
			row.setOnMouseClicked(event -> {
				Eröffnung chooseEröffnung = Eröffnungstabelle.tabelle.getSelectionModel().getSelectedItem();
				System.out.println(chooseEröffnung.getName());

				// Wenn eine andere Eröffnung gewählt wird, reset das Brett
				if (!chooseEröffnung.equals(alteChooseEröffnung)) {
					Main.mainBrettReset();
					Steuerelemente.counter = 0;
					alteChooseEröffnung = chooseEröffnung;

					// Wenn chechBox aktiv ist, Endstellung der Eröffnungen
					if (Steuerelemente.checkBox.isSelected()) {
						Steuerelemente.ende.fire();
						System.out.println("(Endstellung aktiv)");
					}
				}
			});
			return row;
		});

		// Items in Tabelle setzen
		tabelle.setItems(init());

		return tabelle;
	}

	private ObservableList<Eröffnung> init() {
		// Erstelle FXCollection/ObservableList
		eröffnungListe = FXCollections.observableArrayList();

		// Lese Datei.ser aus
		SaveLoad.read();

		return eröffnungListe;
	}
}
