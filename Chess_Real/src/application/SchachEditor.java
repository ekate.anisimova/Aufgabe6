package application;

import static application.Schachbrett.BREITE;
import static application.Schachbrett.H�HE;

import java.util.ArrayList;
import java.util.HashMap;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class SchachEditor {
	public static Stage editorStage;
	public static TextField nameTextField;
	public Schachbrett schachbrett = new Schachbrett();
	public Feld[][] brett = new Feld[BREITE][H�HE];
	public Group feldGroup = new Group();
	public Group figurGroup = new Group();

	public ArrayList<Zug> z�geDerEr�ffnung = new ArrayList<Zug>();
	public HashMap<Integer, Figur> figurFriedhof = new HashMap<Integer, Figur>();

	private Pane root;
	private Pane editorRoot;

	public void resetEditorbrett() {
		// Variablen neu setzen
		schachbrett = new Schachbrett();
		brett = new Feld[BREITE][H�HE];
		feldGroup = new Group();
		figurGroup = new Group();
		nameTextField.clear();
		z�geDerEr�ffnung = new ArrayList<Zug>();

		// Alte Variable entfernen, neue einf�gen
		root.getChildren().remove(editorRoot);
		editorRoot = (Pane) Schachbrett.brettErstellen(brett, feldGroup, figurGroup, true);
		root.getChildren().add(editorRoot);
		editorRoot.relocate(30, 100);
	}

	public void erstellenEditor() {
		// Stage
		editorStage = new Stage();
		editorStage.initModality(Modality.APPLICATION_MODAL);
		root = new Pane();
		root.setPrefSize(580, 720);

		// Header
		Label nameLabel = new Label("Er�ffnungsname");
		nameLabel.relocate(130, 30);
		nameTextField = new TextField();
		nameTextField.setMinWidth(320);
		nameTextField.relocate(130, 50);

		// Editorbrett
		editorRoot = (Pane) Schachbrett.brettErstellen(brett, feldGroup, figurGroup, true);
		editorRoot.relocate(30, 100);

		// Button
		HBox editorBtnBox = (HBox) Steuerelemente.buttonEditor();
		root.getChildren().addAll(nameLabel, nameTextField, editorRoot, editorBtnBox);

		// Scene & Stage Settings
		Scene scene = new Scene(root);
		Image applicationIcon = new Image("/res/w_springer.png");
		editorStage.getIcons().add(applicationIcon);
		editorStage.setTitle("Schacheditor");
		editorStage.setScene(scene);
		editorStage.setResizable(false);
		editorStage.sizeToScene();
		editorStage.showAndWait();
	}

	public void addZ�geDerEr�ffnung(int startX, int startY, int zielX, int zielY) {
		Zug z = new Zug(startX, startY, zielX, zielY);
		z�geDerEr�ffnung.add(z);
	}
}
