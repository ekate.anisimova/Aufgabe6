package application;

import java.io.IOException;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

public class Steuerelemente {
	public static Button hinzufügen, löschen;
	public static CheckBox checkBox;
	public static Button anfang, zurück, weiter, ende;
	public static Button reset, ok;
	public static int counter;

	public HBox buttonTabelle() {
		hinzufügen = new Button("Hinzufügen");
		löschen = new Button("Löschen");
		checkBox = new CheckBox("Endstellung");

		// Eventhandler
		hinzufügen.setOnAction(event -> {
			Main.schachEditor = new SchachEditor();
			Main.schachEditor.erstellenEditor();
		});

		löschen.setOnAction(event -> {
			Eröffnung delEröffnung = Eröffnungstabelle.tabelle.getSelectionModel().getSelectedItem();
			Eröffnungstabelle.eröffnungListe.remove(delEröffnung);
			SaveLoad.write(Eröffnungstabelle.eröffnungListe);

			Main.mainBrettReset();
			counter = 0;
		});

		checkBox.setOnAction(event -> {
			if (checkBox.isSelected()) {
				System.out.println("Endstellung aktiviert.");
				ende.fire();
			} else {
				System.out.println("Endstellung deaktiviert.");
				anfang.fire();
			}
		});

		// Buttons in die HBox
		HBox hbox = new HBox(10, hinzufügen, löschen, checkBox);
		HBox.setMargin(checkBox, new Insets(3, 0, 0, 30));
		hbox.relocate(30, 600);

		return hbox;
	}

	public HBox buttonHauptbrett() throws IOException, ClassNotFoundException {
		Image imgAnfang = new Image("/res/anfang.png", 30, 30, true, true);
		Image imgZurück = new Image("/res/zurück.png", 30, 30, true, true);
		Image imgWeiter = new Image("/res/weiter.png", 30, 30, true, true);
		Image imgEnde = new Image("/res/ende.png", 30, 30, true, true);
		anfang = new Button("", new ImageView(imgAnfang));
		zurück = new Button("", new ImageView(imgZurück));
		weiter = new Button("", new ImageView(imgWeiter));
		ende = new Button("", new ImageView(imgEnde));

		// Eventhandler
		anfang.setOnAction(event -> {
			Main.mainBrettReset();
			counter = 0;
		});

		zurück.setOnAction(event -> {
			// Gewählte Eröffnung auslesen | Counter setzen
			Eröffnungstabelle.wahlEröffnung = Eröffnungstabelle.tabelle.getSelectionModel().getSelectedItem();
			int counter = Eröffnungstabelle.wahlEröffnung.getCounter() - 1;
			Eröffnungstabelle.wahlEröffnung.setCounter(counter);

			if (counter >= 0) {
				try {
					// startX und startY der gewählten Eröffnung
					int startX = Eröffnungstabelle.wahlEröffnung.getZug().get(counter).getStartX();
					int startY = Eröffnungstabelle.wahlEröffnung.getZug().get(counter).getStartY();
					int zielX = Eröffnungstabelle.wahlEröffnung.getZug().get(counter).getZielX();
					int zielY = Eröffnungstabelle.wahlEröffnung.getZug().get(counter).getZielY();

					// Move & Clear
					Figur aktuelleFigur = Main.brett[zielX][zielY].getFigur();
					aktuelleFigur.move(startX, startY);
					Main.brett[zielX][zielY].setFigur(null);
					Main.brett[startX][startY].setFigur(aktuelleFigur);

					// (if): Alte Figur wiederbeleben
					if (Eröffnung.figurFriedhof.containsKey(counter)) {
						Figur reinkarnation = Eröffnung.figurFriedhof.get(counter);
						Main.brett[zielX][zielY].setFigur(reinkarnation);
						Main.figurGroup.getChildren().add(reinkarnation);
						if (Eröffnung.figurFriedhof.get(counter).isFigurWeiss() == true) {
							System.out
									.println("Wiederbeleben von [" + Eröffnung.figurFriedhof.get(counter).getFigurTyp()
											+ ", weiß] auf Position x" + zielX + ",y" + zielY);
						} else {
							System.out
									.println("Wiederbeleben von [" + Eröffnung.figurFriedhof.get(counter).getFigurTyp()
											+ ", schwarz] auf Position x" + zielX + ",y" + zielY);
						}
					}
				} catch (Exception e) {
					System.out.println("Wiederbeleben fehlgeschlagen! Error: " + e.getMessage());
				}
			} else {
				System.out.println("(Anfang des Zuges) | Zug: " + counter);
				Eröffnungstabelle.tabelle.getSelectionModel().getSelectedItem().setCounter(0);
			}
		});

		weiter.setOnAction(event -> {
			// Gewählte Eröffnung auslesen | Counter setzen
			Eröffnungstabelle.wahlEröffnung = Eröffnungstabelle.tabelle.getSelectionModel().getSelectedItem();
			counter = Eröffnungstabelle.wahlEröffnung.getCounter();

			if (counter < Eröffnungstabelle.wahlEröffnung.getZug().size()) {
				try {
					// startX und startY der gewählten Eröffnung
					int startX = Eröffnungstabelle.wahlEröffnung.getZug().get(counter).getStartX();
					int startY = Eröffnungstabelle.wahlEröffnung.getZug().get(counter).getStartY();
					int zielX = Eröffnungstabelle.wahlEröffnung.getZug().get(counter).getZielX();
					int zielY = Eröffnungstabelle.wahlEröffnung.getZug().get(counter).getZielY();

					// Aktuelle Figur erfassen
					Figur aktuelleFigur = Main.brett[startX][startY].getFigur();

					// Temporärer Friedhof: Wenn gegnerische Figur auf dem Feld
					// ist
					if (Main.brett[zielX][zielY].hasFigur()) {
						if (Main.brett[zielX][zielY].getFigur().isFigurWeiss() == true) {
							System.out.println("[" + Main.brett[zielX][zielY].getFigur().getFigurTyp() + " , weiß] x"
									+ zielX + ",y" + zielY + " geschlagen");
						} else {
							System.out.println("[" + Main.brett[zielX][zielY].getFigur().getFigurTyp() + " , schwarz] x"
									+ zielX + ",y" + zielY + " geschlagen");
						}

						Figur andereFigur = Main.brett[zielX][zielY].getFigur();
						Main.figurGroup.getChildren().remove(andereFigur);
						Eröffnung.figurFriedhof.put(counter, andereFigur);
					}

					// Move & Clear
					aktuelleFigur.move(zielX, zielY);
					Main.brett[startX][startY].setFigur(null);
					Main.brett[zielX][zielY].setFigur(null);
					Main.brett[zielX][zielY].setFigur(aktuelleFigur);

					Eröffnungstabelle.wahlEröffnung.setCounter(counter + 1);
				} catch (Exception e) {
					System.out.println("Nächster Zug fehlgeschlagen: " + e.getMessage());
				}
			} else {
				System.out.println("(Ende der Eröffnung) | Zug: " + counter);
			}
		});

		ende.setOnAction(event -> {
			// Zuerst alles resetten mithilfe [anfang]
			anfang.fire();

			// Gewählte Eröffnung auslesen | Fin setzen
			Eröffnungstabelle.wahlEröffnung = Eröffnungstabelle.tabelle.getSelectionModel().getSelectedItem();
			int fin = Eröffnungstabelle.wahlEröffnung.getZug().size();

			// Feuert solange [weiter] bis zur Endstellung
			for (int i = counter; i < fin; i++) {
				weiter.fire();
			}
		});

		// Buttons in die HBox
		HBox hbox = new HBox(0, anfang, zurück, weiter, ende);
		hbox.relocate(518, 595);

		return hbox;
	}

	public static HBox buttonEditor() {
		reset = new Button("Reset");
		ok = new Button("OK");

		// Event
		reset.setOnAction(event -> {
			Main.schachEditor.resetEditorbrett();
		});

		ok.setOnAction(event -> {
			try {
				String eröffnungName = SchachEditor.nameTextField.getText();

				// Eröffnung in die Tabelle hinzufügen
				Eröffnungstabelle.eröffnungListe.add(new Eröffnung(eröffnungName, Main.schachEditor.zügeDerEröffnung));

				// Stage schließen
				SchachEditor.editorStage.close();
				System.out.println("Eröffnung [" + eröffnungName + "] erstellt.");
			} catch (Exception e) {
				System.out.println("Eröffnung erstellen fehlgeschlagen: " + e.getMessage());
			}
			// Datei wird gespeichert
			SaveLoad.write(Eröffnungstabelle.eröffnungListe);
		});

		// Buttons in HBox
		HBox hbox = new HBox(10, reset, ok);
		hbox.relocate(250, 650);

		return hbox;
	}
}
