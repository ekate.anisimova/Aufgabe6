package application;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class Er�ffnung implements Serializable {
	private String name;
	private ArrayList<Zug> zug;
	private int counter = 0;
	public static HashMap<Integer, Figur> figurFriedhof = new HashMap<Integer, Figur>();

	private static final long serialVersionUID = 3907932944766748014L;

	// Konstruktor
	public Er�ffnung(String name, ArrayList<Zug> zug) {
		super();
		this.name = name;
		this.zug = zug;
	}

	// Getter & Setter
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Zug> getZug() {
		return zug;
	}

	public void setZug(ArrayList<Zug> zug) {
		this.zug = zug;
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}
}
