package application;

import static application.Schachbrett.BREITE;
import static application.Schachbrett.HÖHE;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application {
	public static Pane mainRoot, brettRoot;
	public static Schachbrett schachbrett = new Schachbrett();
	public static Feld[][] brett = new Feld[BREITE][HÖHE];
	public static Group feldGroup = new Group();
	public static Group figurGroup = new Group();
	public static SchachEditor schachEditor;

	private Eröffnungstabelle eTabelle = new Eröffnungstabelle();
	private TableView<Eröffnung> tabelle;
	private Steuerelemente buttons = new Steuerelemente();

	public static void mainBrettReset() {
		// Variable neu erstellen
		schachbrett = new Schachbrett();
		brett = new Feld[BREITE][HÖHE];
		feldGroup = new Group();
		figurGroup = new Group();

		// Neue Variablen setzen
		Eröffnungstabelle.tabelle.getSelectionModel().getSelectedItem().setCounter(0);
		mainRoot.getChildren().remove(brettRoot);
		brettRoot = (Pane) Schachbrett.brettErstellen(brett, feldGroup, figurGroup, false);
		mainRoot.getChildren().add(brettRoot);
		brettRoot.relocate(350, 50);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		mainRoot = new Pane();
		mainRoot.setPrefSize(900, 660);

		// Brett
		brettRoot = (Pane) Schachbrett.brettErstellen(brett, feldGroup, figurGroup, false);
		brettRoot.relocate(350, 50);

		// Tabelle
		tabelle = (TableView<Eröffnung>) eTabelle.erstelleTabelle();

		// Buttons
		Group buttonGroup = new Group();
		HBox tabButtons = (HBox) buttons.buttonTabelle();
		HBox brettButtons = (HBox) buttons.buttonHauptbrett();
		buttonGroup.getChildren().addAll(tabButtons, brettButtons);

		mainRoot.getChildren().addAll(brettRoot, tabelle, buttonGroup);

		// Scene & Stage settings
		Scene scene = new Scene(mainRoot);
		Image applicationIcon = new Image("/res/w_springer.png");
		primaryStage.getIcons().add(applicationIcon);
		primaryStage.setTitle("Schachdatenbank für Eröffnungen");
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		primaryStage.sizeToScene();
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
